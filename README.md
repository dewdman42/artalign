This Repo contains a collection of scripts and tools for performing articulation timing alignment when working with Sample libraries that have slow attack transients and inconsistent articulation attack times.

**See the Wiki for full explanation**:  https://gitlab.com/dewdman42/artalign/-/wikis/home


