/***************************************************************************
 * ArtAlignByMSBLSB.js
 *
 * Version:  0.14
 *
 * Articulation Latency Adjustment by CC as MSB/LSB
 * 
 * Docs found here: https://gitlab.com/dewdman42/artalign/-/wikis/home
 *
 ****************************************************************************/
 
/***********************************************************************
 * Optional List of CC switches that should be negative-delayed along with notes
 *
 * Example
 *
 *  var CCSWITCHES = [32,20];
 *
 ***********************************************************************/


var CCSWITCHES = [];


/*****************************************************************
 *****************************************************************
 ************** DO NOT EDIT BELOW HERE ***************************
 *****************************************************************
 *****************************************************************/

var NeedsTimingInfo = true;
var MAXPORTS = 48;
var current;

/**************************
 * HandleMIDI callback
 **************************/

function HandleMIDI(event) {
    event.defaultPort();
    event.latencySend();
}

/*************************************
 * Default port to 1 if not AU3
 *************************************/

Event.prototype.defaultPort = function() {
    if(this.port == undefined || this.port < 1) this.port = 1;
    if(this.port > MAXPORTS) {
        Trace("ERROR: Script only configured for " + MAXPORTS + " ports");
        Trace("       set the MAXPORTS variable to a higher value");
    }
};

/*********************************************
 * PitchBend and AfterTouch, just always send
 * on time, also NoteOff should be on time
 *********************************************/

Event.prototype.latencySend = function() {
    this.setLatency(0);
    this.sendCorrected();
};

/************************************************
 * latencySend method for ControlChange
 * Look for MSB/LSB
 * All other CC's pass through unchanged
 ************************************************/

ControlChange.prototype.latencySend = function() {

    let node = current[this.channel][this.port];
    let ccmode = GuiParameters.get(MODE);

    // MSB/LSB mode
    // TODO - should we check for missing MSB or LSB in this mode?
    if(ccmode==1) {
        if(this.number == GuiParameters.get(LATENCY_LSB)) {      
            if(this.value > 99) node.lsb = 99; // TODO should we bother?
            else node.lsb = this.value;
            node.latency = (node.msb * 100) + node.lsb;
            updateLookahead(node.latency);
            return;
        }
        if(this.number == GuiParameters.get(LATENCY_MSB)) {
            if(this.value > 99) node.msb = 99; // TODO should we bother?
            else node.msb = this.value;
            node.latency = (node.msb * 100) + node.lsb;
            updateLookahead(node.latency);
            return;
        }
    }

    /***********************************
     * Fall through for all other CC's
     ***********************************/

    // CCSWITCH
    if(CCSWITCHES.includes(this.number)) {
        this.setLatency();
        this.sendCorrected();
        return;
    }

    // All other CC's
    this.setLatency(0);
    this.sendCorrected();
};


/*******************************************
 * ProgramChange always send early
 *******************************************/

ProgramChange.prototype.latencySend = function() {
    this.setLatency();
    this.sendCorrected();
};


/*********************************************
 * NoteOn Events may be sent early
 * Unless its actually a NoteOff by velocity 0
 *********************************************/

NoteOn.prototype.latencySend = function() {
    if(this.velocity < 1) {
        this.setLatency(0);  // NoteOff
    }
    else {
        this.setLatency();
    }
    this.sendCorrected();
};

/*******************************************
 * Update the lookahead if set to auto
 *******************************************/

function updateLookahead(latency) {
    if(GuiParameters.get(AUTOLOOK) == 1) {
        let lookahead = GuiParameters.get(LOOKAHEAD);
        if(latency > lookahead) {
            SetParameter(LOOKAHEAD, latency);
            UpdatePluginParameters();
        }
    }
}

/**********************************************
 * Handle events that need to be sent early
 **********************************************/

Event.prototype.sendCorrected = function() {

    let mode = GuiParameters.get(MODE);

    // If disabled, just send it
    if(GuiParameters.get(MODE)==0 ) {
        this.send();
        return;
    }

t     if(GuiParameters.get(ACTIVE)==1) {
        if(GuiParameters.get(CHANNEL)==this.channel) {
            if(MAXPORTS<2) {
                this.send();
                return;
            }
            else {
                if(GuiParameters.get(PORT)== this.port) {
                    this.send();
                    return;
                }
            }
        }
    }

    // if no lookahead, just send it
    let lookahead = GuiParameters.get(LOOKAHEAD);
    if( lookahead == 0 ) {
        this.send();
        return;
    }

    // If this event has zero latency, send with full lookahead delay
    if(this.latency == 0) {
        this.sendAfterMilliseconds(lookahead-1);
        return;
    }

    // Otherwise, subtract known latency from lookahead
    this.sendAfterMilliseconds(lookahead-this.latency);
};

/***************************************************
 *  Set the latency on this event from current
 ***************************************************/

Event.prototype.setLatency = function(ms) {
    if(ms == undefined) {
        this.latency = current[this.channel][this.port].latency;
    }
    else {
        this.latency = ms;
    }
};

/****************************************
 * Initialize callback
 ****************************************/

function Initialize() {

    // setup current[][] tracker
    current = new Array(17);
    for(let chan=1; chan<=16; chan++) {
        current[chan] = new Array(MAXPORTS+1);
        for(let port=1; port<=MAXPORTS; port++) {
            current[chan][port] = {msb:0, lsb:0, latency:0};
        }
    }

    // Make sure GUI is matching mode
    
    SetParameter(MODE, GuiParameters.get(MODE));
    UpdatePluginParameters();
}

function Reset() {
    // TODO, should we reset the current running value?
}

/**********************
 *   ____ _   _ ___ 
 *  / ___| | | |_ _|
 * | |  _| | | || | 
 * | |_| | |_| || | 
 *  \____|\___/|___|
 *                 
 **********************/

// Some enums for GUI items
const MODE         = 0;
const LOOKAHEAD    = 1;
const AUTOLOOK     = 2;
const LATENCY_LSB  = 3;
const LATENCY_MSB  = 4;
const ACTIVE       = 5;
const CHANNEL      = 6;
const PORT         = 7;
 
var PluginParameters = [];

PluginParameters.push({
    name: "Mode",
    type: "menu",
    valueStrings: ["(Disabled)", "Dual CC (MSB/LSB) 0-100"],
    defaultValue: 0,
    hidden: false,
    disableAutomation: true
});

PluginParameters.push({
    name: "Lookahead",
    type: "lin",
    minValue: 0,
    maxValue: 5000,
    numberOfSteps: 5000,
    defaultValue: 0,
    unit: "ms",
    hidden: false,
    disableAutomation: true
});

PluginParameters.push({
    name: "Auto Detect Lookahead",
    type: "checkbox",
    defaultValue: 1,
    hidden: false,
    disableAutomation: true
});


String.prototype.lpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
}

var ccMenu = [];
for(var i = 0;i<MIDI._ccNames.length;i++) {
    ccMenu[i] = "CC-"+i.toString().lpad("0",3)+": "+MIDI._ccNames[i];
}

PluginParameters.push({
    name: "Latency CC# (LSB)",
    type: "menu",
    valueStrings: ccMenu,
    defaultValue: 98,
    hidden: false,
    disableAutomation: true
});

PluginParameters.push({
    name: "Latency CC# (MSB)",
    type: "menu",
    valueStrings: ccMenu,
    defaultValue: 99,
    hidden: false,
    disableAutomation: true
});

PluginParameters.push({
    type: "menu",
    name: "Active Lookahead",
    valueStrings: ["All Channels", "Excluding Record Channel"],
    defaultValue: 0,
    disableAutomation: true,
    hidden: false
});

PluginParameters.push({
    type: "lin",
    name: "MIDI Channel",
    minValue: 1,
    maxValue: 16,
    numberOfSteps: 15,
    defaultValue: 1,
    disableAutomation: true,
    hidden: false
});

if(MAXPORTS>1) {
    PluginParameters.push({
        type: "lin",
        name: "AU3 Port",
        minValue: 1,
        maxValue: 48,
        numberOfSteps: 47,
        defaultValue: 1,
        disableAutomation: true,
        hidden: false
    });
}

var GuiParameters = {
    data: [],
    cache: function(id, val) {
        this.data[id] = val;
    },
    get: function(id) {
        if(this.data[id] == undefined) {
            this.data[id] = GetParameter(id);
        }
        return this.data[id];
    }
};

function ParameterChanged(id, val) {

    if(val == GuiParameters.get(id)) return;

    if(id == ACTIVE ) {
        if( val == 0 ) {
            PluginParameters[CHANNEL].hidden = true;
            if(MAXPORTS>1) PluginParameters[PORT].hidden = true;
        }
        else if ( val == 1 ) {
            PluginParameters[CHANNEL].hidden = false;
            if(MAXPORTS>1) PluginParameters[PORT].hidden = false;
        }
        UpdatePluginParameters();
    }


    // update cache
    GuiParameters.cache(id, val);
}
