/*************************************************************
 * ArtAlign.js
 * v0.17
 *
 * Latency Correction By Port/Channel using JSON specifier
 *
 * Docs found here: https://gitlab.com/dewdman42/artalign/-/wikis/home
 *
 *************************************************************/


/*****************************************************************
 * See online docs for how to create this important json structure
 * that defines your articulation latencies.
 *****************************************************************/


var json = [];



/***************************************
 * User Optional overrides
 ***************************************/

// const LOOKAHEAD = 500;    // 500 ms


/*****************************************************************
 *****************************************************************
 ************** DO NOT EDIT BELOW HERE ***************************
 *****************************************************************
 *****************************************************************/

// declare globals

var chanData;
var portData;
const MAXPORTS = 48;
const MAXMIDI = 16;


/****************************************
 * Just in case user required or optional
 * variables are missing
 ****************************************/

const uLookAhead = 500;   // default
if(typeof LOOKAHEAD != 'undefined') {
    uLookAhead = LOOKAHEAD;
}

var uJson = [];   //default
if(typeof json != 'undefined') {
    uJson = json;   // note this is pointer by ref, not copy
}
else {
    console.log("WARNING, json not provided");
}

/******************************************
 * HandleMIDI Callback
 ******************************************/

function HandleMIDI(event) {
    
    event.defaultPort();
    
    if(isRecordThru(event.port, event.channel)) {
        event.send();
    }
    else {
        event.latencySend();
    }


};


/**************************************
 * check GUI for thru mode
 **************************************/
 
function isRecordThru(inPort, inChan) {

    if(GuiParameters.get(THRU)==1) {
       if( GuiParameters.get(CHANNEL) == inChan) {
       
           if(MAXPORTS>1) {
               if(GuiParameters.get(PORT) == inPort) {
                   return true;
               }
               else {
                   return false;
               }
           }
           else {
               return true;
           }
       }
       else {
           return false;
       }
    }
    else {
        return false;
    }
}

/*************************************
 * Default port to 1 if not AU3
 *************************************/

Event.prototype.defaultPort = function() {
    if(this.port == undefined || this.port < 1) this.port = 1;
    if(this.port > MAXPORTS) {
        Trace("ERROR: script configured for only " + MAXPORTS + " ports");
        Trace("       Set the MAXPORTS variable to larger value");
    }
};

/*********************************************
 * PitchBend and AfterTouch, just always send
 * on time
 *********************************************/

Event.prototype.latencySend = function() {
    this.setLatency(0);
    this.sendCorrected();
};

/************************************************
 * latencySend method for ControlChange
 * flagged CC's go early as switches
 * All other CC's pass through unchanged
 ************************************************/

ControlChange.prototype.latencySend = function() {

    // check to see if flagged as keyswitch, if so then send early
    // like NoteOn
    let config = chanData[this.channel][this.port];  
    
    
    if(config.cc != undefined && config.cc[this.number] == true) {

        // TODO, this is only valid for single velocity mode

        if( config.zones != undefined) {
            console.log(`ERROR: cc switches not allowed when zones are present on ` + 
                        `port ${item.port}, channel ${item.chan}`);
        }

        // TODO, what about zones?  
        this.setLatency(config.lat);
        this.sendCorrected();
        return;
    }

    // Handle XF CC's slighly differently then CC switches, these have
    // to be handled both ways, with some slight concern about that

    if(config.xf != undefined && config.xf[this.number] == true) {

        // TODO, what about zones?  
        this.setLatency(config.lat);
        this.sendCorrected();
        // fall through to also send with 0 correction.
    }

    // All other CC's, full lookahead delay
    this.setLatency(0);
    this.sendCorrected();
};


/*******************************************
 * ProgramChange always send early
 *******************************************/

ProgramChange.prototype.latencySend = function() {
    this.setLatency();
    this.sendCorrected();
};


/***************************************************
 * Handle both NoteOn and NoteOff inside setLatency
 * NoteOn's are early, NoteOff's are at 0 correction
 * unless they have been optionally flagged, detected
 * inside setLatency
 ***************************************************/

Note.prototype.latencySend = function() {
    this.setLatency();
    this.sendCorrected();
};


/**********************************************
 * Handle events that need to be sent early
 **********************************************/

Event.prototype.sendCorrected = function() {

    // if no lookahead, just send it
    if( uLookAhead == undefined || uLookAhead <= 0 ) {
        this.send();
        return;
    }

    // If this event has zero latency, send with full lookahead delay
    if(this.lat == 0) {
        this.sendAfterMilliseconds(uLookAhead-1);
        return;
    }

    // Otherwise, subtract known latency from lookahead
    // TODO, should this be -1?
    this.sendAfterMilliseconds(uLookAhead-this.lat);
};



/***************************************************
 *  Set the latency on this event from current
 *  Applies to everyething except NoteOn
 ***************************************************/

Event.prototype.setLatency = function(ms) {
    if(ms == undefined) {
        let config = chanData[this.channel][this.port];    
        this.lat = config.lat;
    }
    else {
        this.lat = ms;
    }
};


// TODO - should probably update this so that it will detect
//        legato notes EVEN if the previous note did not overlap
//        if the corrected start time will end up making it overlap.
//        NEED to think this through a little bit.

// For NoteOn, use Note Velocity to determine latency.
NoteOn.prototype.setLatency = function(ms) {

    // just in case stealthy NoteOff
    if(this.velocity < 1) {
        return this._noteOffLatency(ms);
    }

    // update the last Channel
    let last = portData[this.port].lastChannel;
    portData[this.port].lastChannel = this.channel;
   
    // is ms provided, use it and get out
    if(ms != undefined) {
        this.lat = ms;
        return;
    }
    
    let config = chanData[this.channel][this.port];

    // if no zones, just use global for channel
    if(config.zones == undefined) {
    
        // if leg2 present
        if( config.leg2 != undefined) {
        
            if(last == this.channel) {                    
                this.lat = config.leg2;
            }             
            else {
                this.lat = config.lat;
            }
        }
        
        // no leg2, so just use the one latency value
        else {
            this.lat = config.lat;
        }

    }    

    // there are zones, so use it.
    else {
        let zone = config.zones[this.velocity];

        // If Leg2 present
        if( zone.leg2 != undefined) {
        
           if(last == this.channel ) {          
                this.lat = zone.leg2;
            }             
            else {
                this.lat = zone.lat;
            }
        }
        
        // no Leg2 so just use lat
        else {
            this.lat = zone.lat;
        }
    }
    

};

Note.prototype._noteOffLatency = function(ms) {

    // is ms provided, use it and get out
    if(ms != undefined) {
        this.lat = ms;
        return;
    }
    
    let config = chanData[this.channel][this.port];

    // if no zones, just use global for channel
    if(config.zones == undefined) {
        if( config.off == undefined) this.lat = 0;
        else event.lat = config.off;
    }    

    // there are zones, so use it.
    else {
        let zone = config.zones[this.velocity];
        if (zone.off == undefined) this.lat = 0;
        else this.lat = zone.off;
    }
};

NoteOff.prototype.setLatency = function(ms) {
    return this._noteOffLatency(ms);
};



/**********************
 *   ____ _   _ ___ 
 *  / ___| | | |_ _|
 * | |  _| | | || | 
 * | |_| | |_| || | 
 *  \____|\___/|___|
 *                 
 **********************/
 
// Some enums for the GUI

var PluginParameters = [];

PluginParameters.push({
    type: "checkbox",
    name: "Record-Thru",
    defaultValue: 0,
    disableAutomation: false,
    hidden: false
});
const THRU = PluginParameters.length-1;

if(MAXPORTS > 1) {
    PluginParameters.push({
        type: "lin",
        name: "MIDI Port",
        minValue: 1,
        maxValue: MAXPORTS,
        numberOfSteps: MAXPORTS-1,
        defaultValue: 1,
        disableAutomation: false,
        hidden: false
    });
}
const PORT = PluginParameters.length-1;


PluginParameters.push({
    type: "lin",
    name: "MIDI Channel",
    minValue: 1,
    maxValue: MAXMIDI,
    numberOfSteps: MAXMIDI-1,
    defaultValue: 1,
    disableAutomation: false,
    hidden: false
});
const CHANNEL = PluginParameters.length-1;


PluginParameters.push({
    name: "──── Options ────",
    type: "text"
});
    
PluginParameters.push({
    type: "lin",
    name: "Detected",
    minValue: 0,
    maxValue: uLookAhead,
    numberOfSteps: uLookAhead,
    defaultValue: 0,
    disableAutomation: true,
    readOnly: true,
    unit: "ms",
    hidden: false
});
const DETECTED = PluginParameters.length-1;

PluginParameters.push({
    type: "lin",
    name: "Lookahead",
    minValue: 0,
    maxValue: uLookAhead,
    numberOfSteps: uLookAhead,
    defaultValue: uLookAhead,
    disableAutomation: true,
    readOnly: true,
    unit: "ms",
    hidden: false
});
const LOOKDISP = PluginParameters.length-1;

function ParameterChanged(id, val) {
    GuiParameters.cache(id, val);
}

var GuiParameters = {
    data: [],
    cache: function(id, val) {
        this.data[id] = val;
    },
    get: function(id) {
        if(this.data[id] == undefined) {
            this.data[id] = GetParameter(id);
        }
        return this.data[id];
    },
    check: function(id) {
        return this.data[id];
    }
};


/*************************************
 * bootstrap initialization once upon 
 * first Idle
 *************************************/
var bootstrapped=false;

function bootstrap() {

    bootstrapped = true;
    
    /*************************************
     * init internal arrays
     *************************************/
     
    chanData = new Array(MAXMIDI+1);
    for(let chan=1;chan<=MAXMIDI;chan++) {
        chanData[chan] = new Array(MAXPORTS+1);    
        for(let port=1;port<=MAXPORTS;port++) {
            chanData[chan][port] = {lat:0};
        }
    }
    
    portData = new Array(MAXPORTS+1);
    for(let port=1;port<=MAXPORTS;port++) {
        portData[port] = {lastChannel:0};
    }

    /************************************************
     * copy in user provided latency values from user
     * provided JSON
     ************************************************/

    for(let i=0;i<uJson.length;i++) {

        let item = uJson[i];
        
        if(item.port == undefined || item.port == 0) {
            item.port = 1;
        }
        else if( item.port < 1 || item.port > MAXPORTS) {
            console.log("ERROR, invalid PORT value: " + JSON.stringify(item));
            continue;
        }
        
        if(item.chan == undefined 
                || item.chan < 1 
                || item.chan > MAXMIDI) {
            item.chan = 1;
            console.log("WARNING, channel parameter not provided, assuming channel 1");
        }
        
        let chanFig = chanData[item.chan][item.port];

        // not a valid entry, skip it
        if(item.lat==undefined && item.zones==undefined) {
            continue;
        }
        
        if(item.lat != undefined) {
            // TODO, do sanity checking to make sure its a valid latency int
            chanFig.lat = item.lat;
        }

        if(item.leg2 != undefined) {
            // TODO, do sanity checking to make sure its a valid latency int
            chanFig.leg2 = item.leg2;
        }

        if(item.off != undefined) {
            // TODO, do sanity checking to make sure its a valid latency int
            chanFig.off = item.off;
        }

        // There is a zones array, so handle it
        if(item.zones != undefined) {

            console.log(`WARNING: Zones found on port ${item.port}, channel ${item.chan}`);
            console.log("         do not send any keyswitches on that channel");

            chanFig.zones = new Array(128);

            // if there is a global latency for the channel, init zones to that
            // otherwise use zero
            for(let z=0; z<128; z++) {
                chanFig.zones[z] = {};
                let zone = chanFig.zones[z];
                if (item.lat != undefined) {
                    zone.lat = item.lat;
                }
                else {
                    zone.lat = 0;
                }
                if (item.leg2 != undefined) {
                    zone.leg2 = item.leg2;
                }
                if (item.off != undefined) {
                    zone.off = item.off;
                }
            }
           
            // Go through the json zones and populate their latencies 
            for(let z=0; z<item.zones.length; z++) {
                
                let izone = item.zones[z];
                
                if(izone.lat == undefined) {
                     console.log(`ERROR: Zone missing latency definition, skipping zone`);
                }
                else if(izone.vel == undefined || izone.vel.length != 2) {
                     console.log(`ERROR: Zone missing vel range definition [n,n], skipping zone`);               
                }
                
                else {
                    let range = izone.vel;
                    for(let r=range[0]; r<range[1]; r++) {
                    
                        let zone = chanFig.zones[r];
                        zone.lat = izone.lat;
                        if(izone.leg2 != undefined) {
                            zone.leg2 = izone.leg2;
                        }
                        if(izone.off != undefined) {
                            zone.off = izone.off;
                        }

                    }
                }
            }
        }
        

        // If there are CC switches defined, init them into cc array
        chanFig.cc = new Array(128);
        
        for( let c=0;c<128;c++) {
            chanFig.cc[c] = false;        
        }
        
        if(item.cc != undefined ) {

            // check for existence of zones, which would probably be error
            if(item.zones != undefined) {
                console.log(`ERROR: cc switches and zones specified on port ${item.port}, `
                            `channel ${item.chan}`);
                console.log("       do not send any keyswitches on that channel");
            }

            for( let e=0; e<item.cc.length; e++) {
                if(item.cc[e] < 1 || cc[e] > 127) {
                    console.log("ERROR, invalid CCnum: " + JSON.stringify(item));
                    continue;
                }
                chanFig.cc[item.cc[e]] = true;
            }

        // If there are XF VelocityXfade type expressions defined, init them into xf array
        // These will be handled in a special way where the current value of XF will go
        // be pushed early in front of each note, but then will continue to be sent 
        // with zero latency correction like all expression CC.

        chanFig.xf = new Array(128);
        
        for( let c=0;c<128;c++) {
            chanFig.xf[c] = false;        
        }
        
        if(item.xf != undefined ) {

            for( let e=0; e<item.xf.length; e++) {
                if(item.xf[e] < 1 || xf[e] > 127) {
                    console.log("ERROR, invalid XF CCnum: " + JSON.stringify(item));
                    continue;
                }
                chanFig.xf[item.xf[e]] = true;
            }
        }
        }
    } // end json

    /************************************************************
     * Scan Array to find largest latency and report as detected
     ************************************************************/
    let detected = 0;
    for(let chan=1;chan<=MAXMIDI;chan++) {
        for(let port=1;port<=MAXPORTS;port++) {
        
            let chanFig = chanData[chan][port];

            let glat = chanFig.lat;
            let gleg2 = chanFig.leg2;
            let goff = chanFig.off;

            if(glat != undefined && glat > detected) {
                detected = glat;
            }

            if(gleg2 != undefined && gleg2 > detected) {
                detected = gleg2;
            }

            if(goff != undefined && goff > detected) {
                detected = goff;
            }    
            
            // Now check the zones
            if(chanFig.zones != undefined) {
                for(let r=0; r<128; r++) {
                    let zone = chanFig.zones[r];
                    let zlat = zone.lat;
                    let zleg2 = zone.leg2;
                    let zoff = zone.off;

                    if(zlat != undefined && zlat > detected) {
                        detected = zlat;
                    }
                    if(zleg2 != undefined && zleg2 > detected) {
                        detected = zleg2;
                    }
                    if(zoff != undefined && zoff > detected) {
                        detected = zoff;
                    }
                }
            }
        }
    }
 
    SetParameter(DETECTED, detected);
}

/*******************
 * buffered logging
 *******************/

var console = {
    maxFlush: 20,
    b:[],
    log: function(msg) {this.b.push(msg)},
    flush: function() {
        let i=0;
        while(i<=this.maxFlush && this.b.length>0) {
            Trace(this.b.shift());
            i++;
        }
    }
};


function Idle() {
    console.flush();
    if(!bootstrapped) bootstrap();
}
