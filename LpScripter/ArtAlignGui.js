 /*************************************************************
 * ArtAlignGui.js
 * v0.10 beta
 *
 * Latency Correction By Port/Channel
 * This version uses GUI to specify the latency values per channel
 *
 * Docs found here: https://gitlab.com/dewdman42/artalign/-/wikis/home
 *
 *************************************************************/

/******************************************
 *  User declarations
 ******************************************/

const LOOKAHEAD = 500;   // ms
const MAXPORTS  = 1;
 
/*****************************************************************
 *****************************************************************
 ************** DO NOT EDIT BELOW HERE ***************************
 *****************************************************************
 *****************************************************************/

// declare globals

const MAXMIDI = 16;


/******************************************
 * HandleMIDI Callback
 ******************************************/

function HandleMIDI(event) {
   
    event.defaultPort(); // TODO do we reall need this?

    if(isRecordThru(event.port, event.channel)) {
        event.send();
    }
    else {
        if(GuiParameters.get(idSIMPLE)==1) {
            event.latencySendSimple();
        }
        else {
            event.latencySend();
        }
    }
}

/**************************************
 * check GUI for thru mode
 **************************************/
 
function isRecordThru(inPort, inChan) {

    if(GuiParameters.get(idTHRU)==1) {
       if( GuiParameters.get(idCHANNEL) == inChan) {
       
           if(MAXPORTS>1) {
               if(GuiParameters.get(idPORT) == inPort) {
                   return true;
               }
               else {
                   return false;
               }
           }
           else {
               return true;
           }
       }
       else {
           return false;
       }
    }
    else {
        return false;
    }
}


/*************************************
 * Default port to 1 if not AU3
 *************************************/

Event.prototype.defaultPort = function() {
    if(this.port == undefined || this.port < 1) this.port = 1;
    if(this.port > MAXPORTS) {
        Trace("ERROR: script configured for only " + MAXPORTS + " ports");
        Trace("       Set the MAXPORTS variable to larger value");
    }
};


/*************************************
 * When idSIMPLE mode is enabled (GUI)
 * always send early
 *************************************/

Event.prototype.latencySendSimple = function() {
    this.setLatency();
    this.sendCorrected();
};


/*********************************************
 * PitchBend and AfterTouch, CC just always send
 * on time, also NoteOff should be on time
 * This will catch them all
 *********************************************/

Event.prototype.latencySend = function() {
    this.setLatency(0);
    this.sendCorrected();
};


/*******************************************
 * ProgramChange always send early
 *******************************************/

ProgramChange.prototype.latencySend = function() {
    this.setLatency();
    this.sendCorrected();
};


/*********************************************
 * NoteOn Events may be sent early
 * Unless its actually a NoteOff by velocity 0
 *********************************************/

NoteOn.prototype.latencySend = function() {
    if(this.velocity < 1) {
        this.setLatency(0);  // NoteOff
    }
    else {
        this.setLatency();
    }
    this.sendCorrected();
};


/***************************************************
 *  Set the latency on this event from current
 *  Applies to everything except NoteOn
 ***************************************************/

Event.prototype.setLatency = function(ms) {
    if(ms == undefined) { 
        this.lat = GuiParameters.get(idxFinder[this.port][this.channel]);
    }
    else {
        this.lat = ms;
    }
};




/**********************************************
 * Handle events that need to be sent early
 **********************************************/

Event.prototype.sendCorrected = function() {

    // if no lookahead, just send it
    if( LOOKAHEAD == 0 ) {
        this.send();
        return;
    }

    // If this event has zero latency, send with full lookahead delay
    if(this.lat == 0) {
        this.sendAfterMilliseconds(LOOKAHEAD-1);
        return;
    }

    // Otherwise, subtract known latency from lookahead, should this be -1?
    this.sendAfterMilliseconds(LOOKAHEAD-this.lat);
};




/**********************
 *   ____ _   _ ___ 
 *  / ___| | | |_ _|
 * | |  _| | | || | 
 * | |_| | |_| || | 
 *  \____|\___/|___|
 *                 
 **********************/
 
// Some enums for the GUI

var PluginParameters = [];


PluginParameters.push({
    type: "checkbox",
    name: "Record Thru",
    defaultValue: 0,
    disableAutomation: false,
    hidden: false
});
const idTHRU = PluginParameters.length-1;

if(MAXPORTS > 1) {
    PluginParameters.push({
        type: "lin",
        name: "MIDI Port",
        minValue: 1,
        maxValue: MAXPORTS,
        numberOfSteps: MAXPORTS-1,
        defaultValue: 1,
        disableAutomation: false,
        hidden: false
    });
}
const idPORT = PluginParameters.length-1;


PluginParameters.push({
    type: "lin",
    name: "MIDI Channel",
    minValue: 1,
    maxValue: MAXMIDI,
    numberOfSteps: MAXMIDI-1,
    defaultValue: 1,
    disableAutomation: false,
    hidden: false
});
const idCHANNEL = PluginParameters.length-1;


PluginParameters.push({
    name: "──── Options ────",
    type: "text"
});

PluginParameters.push({
    type: "checkbox",
    name: "Adjust NoteOff",
    defaultValue: 1,
    disableAutomation: false,
    hidden: false
});
const idSIMPLE = PluginParameters.length-1;

PluginParameters.push({
    type: "lin",
    name: "Detected",
    defaultValue: 0,
    minValue: 0,
    maxValue: LOOKAHEAD,
    numberOfSteps: LOOKAHEAD,
    unit: "ms",
    readOnly: true,
    hidden: false,
    disableAutomation: true
});
const idDETECTED = PluginParameters.length-1;


PluginParameters.push({
    type: "lin",
    name: "Lookahead",
    defaultValue: LOOKAHEAD,
    minValue: 0,
    maxValue: LOOKAHEAD,
    numberOfSteps: LOOKAHEAD,
    unit: "ms",
    readOnly: true,
    hidden: false,
    disableAutomation: true
});
const idLOOKAHEAD = PluginParameters.length-1;


PluginParameters.push({
    name: " ",
    type: "text"
});
PluginParameters.push({
    name: " ",
    type: "text"
});
PluginParameters.push({
    name: " ",
    type: "text"
});
PluginParameters.push({
    name: " ",
    type: "text"
});


PluginParameters.push({
    name: "──── Channel Latency ────",
    type: "text"
});


var portList = ["Select Port..."];
for(let port=1; port<=MAXPORTS; port++) {
    portList.push(port.toString());
}

PluginParameters.push({
    type: "menu",
    name: "Port",
    valueStrings: portList,
    defaultValue: 1,
    disableAutomation: true,
    hidden: false
});
const idPORTMENU = PluginParameters.length-1;

for(let port=1; port<=MAXPORTS; port++) {
    for(let chan=1; chan<=MAXMIDI; chan++) {

        PluginParameters.push({
            type: "lin",
            name: `P${port} Channel ${chan}`,
            minValue: 0,
            maxValue: LOOKAHEAD,
            numberOfSteps: LOOKAHEAD,
            defaultValue: 0,
            disableAutomation: true,
            hidden: true
        });
    }
}
const idFIRST = idPORTMENU+1;
const idLAST = PluginParameters.length-1;

/******************************************************
 * Create lookup table to find the right GUI parameter
 * per channel
 ******************************************************/

var idxFinder = new Array(MAXPORTS+1);
for( let p=0; p<idxFinder.length; p++) {
    idxFinder[p] = new Array(MAXMIDI+1);
    for( let c=0; c<idxFinder[p].length; c++) {
        idxFinder[p][c] = idFIRST + ((p-1)*MAXMIDI) + c-1;
    }
}

/*******************************************
 * ParameterChanged callback
 *******************************************/
 
function ParameterChanged(id, val) {

    // skip if value not changing
    if(GuiParameters.check(id) == val) return;  
    
    GuiParameters.cache(id, val);

    if(id == idDETECTED ) return; // readonly

    else if(id == idPORTMENU && val != 0) {
        updatePortGUI(val);
    }
    
    else if(id >= idFIRST && id <= idLAST) {
        updateLookahead(val);
    }
}


/*******************************************
 * Change which Channel sliders are showing
 *******************************************/


function updatePortGUI(val) {

    if(val == 0) return;   // work around bug in Scripter
    
    let start = ((val-1) * MAXMIDI) + idFIRST;
    
    for(let i=idFIRST; i<=idLAST; i++) {
        if(i >= start && i < start+MAXMIDI) {
            showCtrl(i);
        }
        else {
            hideCtrl(i);
        }
    }
    
    // UpdatePluginParameters deferred to Idle
}

/*******************************************
 * Update the idDETECTED field with largest
 * latency found
 *******************************************/
 
function updateLookahead(val) {
    // Can't ignore zero, not sure the implication
    let current = GuiParameters.get(idDETECTED);
    
    if(val > GuiParameters.get(idDETECTED)) {
        SetParameter(idDETECTED, val);
    }
    else if(val < current) {
        let max=0;
        for(let i=idFIRST; i<=idLAST; i++) {
            let v = GuiParameters.get(i);
            if(v > max ) {
                max = v;
            }
        }
        if (max != current) {
            SetParameter(idDETECTED, max);
        }
    }
}


/**************************
 * cached Parameters
 **************************/
 
var GuiParameters = {
    data: [],
    cache: function(id, val) {
        this.data[id] = val;
    },
    get: function(id) {
        if(this.data[id] == undefined) {
            this.data[id] = GetParameter(id);
        }
        return this.data[id];
    },
    check: function(id) {
        return this.data[id];
    }
};


/*******************
 * buffered logging
 *******************/

var console = {
    maxFlush: 18,
    b:[],
    log: function(msg) {this.b.push(msg)},
    flush: function() {
        let i=0;
        while(i<=this.maxFlush && this.b.length>0) {
            Trace(this.b.shift());
            i++;
        }
    }
};


/*******************************************
 * SHOW parameter control
 *******************************************/
 
function showCtrl(id) {
    if(PluginParameters[id].hidden != false) {
        PluginParameters[id].hidden = false;
        guiPaint = true;
    }
}


/*******************************************
 * HIDE parameter control
 *******************************************/
 
function hideCtrl(id) {
    if(PluginParameters[id].hidden != true) {
        PluginParameters[id].hidden = true;
        guiPaint = true;
    }
}


/*******************************************
 * Idle Callback
 *******************************************/

var guiPaint = false;
var bootstrapped = false;
function Idle() {
 
    // Bootstrap some parameters one time at last possible stage
    if (!bootstrapped) {
        SetParameter(idPORTMENU,1);
        bootstrapped=true;
    }    

    if(guiPaint) {
        guiPaint = false;
        UpdatePluginParameters();
    }

    // flush buffered logging
    console.flush();
}

