/***************************************************************************
 * ArtAlignByLSB.js
 *
 * Version:  0.13
 *
 * Articulation Latency Adjustment by CC as single LSB value + multiplier
 * 
 * Docs found here: https://gitlab.com/dewdman42/artalign/-/wikis/home
 *
 ****************************************************************************/
 
/***********************************************************************
 * Optional List of CC switches that should be negative-delayed along with notes
 *
 * Example
 *
 *  var CCSWITCHES = [32,20];
 *
 ***********************************************************************/


var CCSWITCHES = [];


/*****************************************************************
 *****************************************************************
 ************** DO NOT EDIT BELOW HERE ***************************
 *****************************************************************
 *****************************************************************/

var NeedsTimingInfo = true;
var MAXPORTS = 48;
var current;

/**************************
 * HandleMIDI callback
 **************************/

function HandleMIDI(event) {
    event.defaultPort();
    event.latencySend();
}

/*************************************
 * Default port to 1 if not AU3
 *************************************/

Event.prototype.defaultPort = function() {
    if(this.port == undefined || this.port < 1) this.port = 1;
};

/*********************************************
 * PitchBend and AfterTouch, just always send
 * on time, also NoteOff should be on time
 *********************************************/

Event.prototype.latencySend = function() {
    this.setLatency(0);
    this.sendCorrected();
};

/************************************************
 * latencySend method for ControlChange
 * If CC98, remember the value, don't send
 * If in list of CCSWITCHES, send early
 * All other CC's pass through unchanged
 ************************************************/

ControlChange.prototype.latencySend = function() {

    let node = current[this.channel][this.port];
    let ccmode = GuiParameters.get(MODE);

    // multipler mode
    if(ccmode==1) {
        // check for latency CC LSB
        if(this.number == GuiParameters.get(LATENCY_LSB)) {
            node.lsb = this.value;
            node.latency = this.value * (GuiParameters.get(RANGE)+1);
            updateLookahead(node.latency);
            return;
        }
    }

    /***********************************
     * Fall through for all other CC's
     ***********************************/

    // CCSWITCH
    if(CCSWITCHES.includes(this.number)) {
        this.setLatency();
        this.sendCorrected();
        return;
    }

    // All other CC's
    this.setLatency(0);
    this.sendCorrected();
};


/*******************************************
 * ProgramChange always send early
 *******************************************/

ProgramChange.prototype.latencySend = function() {
    this.setLatency();
    this.sendCorrected();
};


/*********************************************
 * NoteOn Events may be sent early
 * Unless its actually a NoteOff by velocity 0
 *********************************************/

NoteOn.prototype.latencySend = function() {
    if(this.velocity < 1) {
        this.setLatency(0);  // NoteOff
    }
    else {
        this.setLatency();
    }
    this.sendCorrected();
};

/*******************************************
 * Update the lookahead if set to auto
 *******************************************/

function updateLookahead(latency) {
    if(GuiParameters.get(AUTOLOOK) == 1) {
        let lookahead = GuiParameters.get(LOOKAHEAD);
        if(latency > lookahead) {
            SetParameter(LOOKAHEAD, latency);
            UpdatePluginParameters();
        }
    }
}

/**********************************************
 * Handle events that need to be sent early
 **********************************************/

Event.prototype.sendCorrected = function() {

    let mode = GuiParameters.get(MODE);

    // If disabled, just send it
    if(GuiParameters.get(MODE)==0 ) {
        this.send();
        return;
    }

    if(GuiParameters.get(ACTIVE)==1) {
        if(GuiParameters.get(CHANNEL)==this.channel) {
            if(MAXPORTS<2) {
                this.send();
                return;
            }
            else {
                if(GuiParameters.get(PORT)== this.port) {
                    this.send();
                    return;
                }
            }
        }
    }

    // if no lookahead, just send it
    let lookahead = GuiParameters.get(LOOKAHEAD);
    if( lookahead == 0 ) {
        this.send();
        return;
    }

    // If this event has zero latency, send with full lookahead delay
    if(this.latency == 0) {
        this.sendAfterMilliseconds(lookahead-1);
        return;
    }

    // Otherwise, subtract known latency from lookahead
    this.sendAfterMilliseconds(lookahead-this.latency);
};

/***************************************************
 *  Set the latency on this event from current
 ***************************************************/

Event.prototype.setLatency = function(ms) {

    if(ms == undefined) {

        this.latency = current[this.channel][this.port].latency;

        // if multiplier mode, then possibly humanize

        if(GuiParameters.get(MODE)==1 && GuiParameters.get(HUMANIZE)==1) {
            let factor = GuiParameters.get(RANGE)+1;
            // if there is a multiple greater then 2
            if( factor > 2 && this.latency > 0 ) {
                let offset = Math.round ( Math.random() * factor ) - Math.round(factor/2);
                this.latency += offset;
                //console.log(`Offset=${offset}; factor=${factor}; latency=${this.latency}`);
            }
        }
    }
    else {
        this.latency = ms;
    }
};

/****************************************
 * Initialize callback
 ****************************************/

function Initialize() {

    // setup current[][] tracker
    current = new Array(17);
    for(let chan=1; chan<=16; chan++) {
        current[chan] = new Array(MAXPORTS+1);
        for(let port=1; port<=MAXPORTS; port++) {
            current[chan][port] = {msb:0, lsb:0, latency:0};
        }
    }

    // Make sure GUI is matching mode
    
    SetParameter(MODE, GuiParameters.get(MODE));
    UpdatePluginParameters();
}

function Reset() {
    // TODO, should we reset the current running value?
}

/**********************
 *   ____ _   _ ___ 
 *  / ___| | | |_ _|
 * | |  _| | | || | 
 * | |_| | |_| || | 
 *  \____|\___/|___|
 *                 
 **********************/

// Some enums for GUI items
const MODE         = 0;
const LOOKAHEAD    = 1;
const AUTOLOOK     = 2;
const LATENCY_LSB  = 3;
const RANGE        = 4;
const HUMANIZE     = 5;
const ACTIVE       = 6;
const CHANNEL      = 7;
const PORT         = 8;
 
var PluginParameters = [];

PluginParameters.push({
    name: "Mode",
    type: "menu",
    valueStrings: ["(Disabled)", "Single CC (LSB) with multiplier"],
    defaultValue: 0,
    hidden: false,
    disableAutomation: true
});

PluginParameters.push({
    name: "Lookahead",
    type: "lin",
    minValue: 0,
    maxValue: 1270,
    numberOfSteps: 1270,
    defaultValue: 0,
    unit: "ms",
    hidden: false,
    disableAutomation: true
});

PluginParameters.push({
    name: "Auto Detect Lookahead",
    type: "checkbox",
    defaultValue: 1,
    hidden: false,
    disableAutomation: true
});


String.prototype.lpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
}

var ccMenu = [];
for(var i = 0;i<MIDI._ccNames.length;i++) {
    ccMenu[i] = "CC-"+i.toString().lpad("0",3)+": "+MIDI._ccNames[i];
}

PluginParameters.push({
    name: "Latency CC# (LSB)",
    type: "menu",
    valueStrings: ccMenu,
    defaultValue: 98,
    hidden: false,
    disableAutomation: true
});

PluginParameters.push({
    name: "Latency Range",
    type: "menu",
    valueStrings: ["1x: 0ms - 127ms","2x: 0ms - 254ms",
                   "3x: 0ms - 381ms","4x: 0ms - 508ms",
                   "5x: 0ms - 635ms","6x: 0ms - 762ms",
                   "7x: 0ms - 889ms","8x: 0ms - 1016ms",
                   "9x: 0ms - 1143ms","10x: 0ms - 1270ms",

    ],
    defaultValue: 0,
    hidden: false,
    disableAutomation: true
});

PluginParameters.push({
    name: "Humanize Large Range",
    type: "checkbox",
    defaultValue: 1,
    hidden: false,
    disableAutomation: true
});

PluginParameters.push({
    type: "menu",
    name: "Active Lookahead",
    valueStrings: ["All Channels", "Excluding Record Channel"],
    defaultValue: 0,
    disableAutomation: true,
    hidden: false
});

PluginParameters.push({
    type: "lin",
    name: "MIDI Channel",
    minValue: 1,
    maxValue: 16,
    numberOfSteps: 15,
    defaultValue: 1,
    disableAutomation: true,
    hidden: false
});

if(MAXPORTS>1) {
    PluginParameters.push({
        type: "lin",
        name: "AU3 Port",
        minValue: 1,
        maxValue: 48,
        numberOfSteps: 47,
        defaultValue: 1,
        disableAutomation: true,
        hidden: false
    });
}


var GuiParameters = {
    data: [],
    cache: function(id, val) {
        this.data[id] = val;
    },
    get: function(id) {
        if(this.data[id] == undefined) {
            this.data[id] = GetParameter(id);
        }
        return this.data[id];
    }
};

function ParameterChanged(id, val) {

    if( val == GuiParameters.get(id)) return;

    let dirty = false;

    if(id == ACTIVE ) {
        if( val == 0 ) {
            PluginParameters[CHANNEL].hidden = true;
            if(MAXPORTS>1) PluginParameters[PORT].hidden = true;
        }
        else if ( val == 1 ) {
            PluginParameters[CHANNEL].hidden = false;
            if(MAXPORTS>1) PluginParameters[PORT].hidden = false;
        }
        dirty = true;
    }

    // If they are changing the multiplier, then reset the lookahead ms
    if(id == RANGE) {
        let factor = GuiParameters.get(RANGE);
        if (val != factor) {
            if(GuiParameters.get(AUTOLOOK)==1) {
                factor += 1;
                let lookahead = GuiParameters.get(LOOKAHEAD);
                let newVal = Math.round(lookahead/factor) * (val+1);
                SetParameter(LOOKAHEAD, newVal);
                dirty = true;
            }
        }
    }

    // update cache
    GuiParameters.cache(id, val);
    
    if(dirty) UpdatePluginParameters();
}
