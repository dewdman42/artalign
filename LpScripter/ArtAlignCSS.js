/*********************************************************************
 * ArtAlignCSS.js
 *
 * v0.03
 *
 * Custom Script for Cinematic Studio Strings
 *
 * Hand coded script for handling keyswitching and latency correction
 * of Cinematic Studio Strings
 *
 ********************************************************************/

// TODO make sure the CCXF, CCPORT, CCNV, CC58 and Mute keyswitch can all get 
//      throuugh to the instrument with the right timing.
//
// TODO getting hanging note sometimes.  Why?
//
// TODO what's wrong with measured trem
//
// TODO maracato timing doesn't feel right, what is going on there?


const LOOKAHEAD = 500;

// Latencies
const SHORT      = 60;
const ZERO       = 0;

// Legato Latencies
const PORTAMENTO = 300;
const SLOW       = 300;
const MEDIUM     = 250;
const FAST       = 100;

const CCXF       = 1;
const CCPORT     = 5;
const CCNV       = 2;


// when these are present, only process on these channels

// var onlyPort=1;
// var onlyChannels=[1,2,3,4,5];


/*************************************************************
 *************************************************************
 *************** DO NOT EDIT BELOW HERE **********************
 *************************************************************
 *************************************************************/


const MAXPORTS = 48;
const MAXMIDI = 16;

var lastArt = new Array(MAXPORTS+1);
for(let p=1; p<lastArt.length; p++) {
    lastArt[p] = new Array(MAXMIDI+1);
}

var lastXF = new Array(MAXPORTS+1);
for(let p=1; p<lastXF.length; p++) {
    lastXF[p] = new Array(MAXMIDI+1);
}

var lastPortamento = new Array(MAXPORTS+1);
for(let p=1; p<lastPortamento.length; p++) {
    lastPortamento[p] = new Array(MAXMIDI+1);
}

/**************************************
 * HandleMIDI
 **************************************/

function HandleMIDI(event) {

    event.defaultPort();

    if(event.port != onlyPort 
           && onlyChannels.indexOf(event.channel) == -1 ) {
           
        event.sendAfterMilliseconds(LOOKAHEAD);
        return;
    }


    if(isRecordThru(event.port, event.channel)) {
        event.send();
    }
    else {
        event.handle();
    }
}

/**************************************
 * isRecordThru()
 **************************************/
 
function isRecordThru(inPort, inChan) {

    if(GuiParameters.get(THRU)==1) {
        if( GuiParameters.get(CHANNEL) == inChan
                && GuiParameters.get(PORT) == inPort){
            return true;
        }
    }
    
    else return false;
}

/**************************************
 * Event.defaultPort()
 **************************************/

Event.prototype.defaultPort = function() {
    if(this.port == undefined || this.port < 1) this.port = 1;
};

/**************************************
 * Event types handlers
 **************************************/

/************************************************
 * Handle AfterTouch, PitchBend, NoteOff
 **/
Event.prototype.handle = function() {
    this.sendAfterMilliseconds(LOOKAHEAD);
};

// Handle PC messages
ProgramChange.prototype.handle = function() {
    // program user error, just send it through immediately
    // TODO, or we could push them to a stack and release with next note
    this.send();
};

// Handle CC messages
ControlChange.prototype.handle = function() {
    
    // Known instrument switch, user error, send immediately when encountered
    // TODO, another way would be to push it into an array and spray
    // the array when the next note comes on a channel
    if(this.number == 58) {
        this.send();
        return;
    }
    
    // TODO look for Mute controller particularly
    
    // Mod wheel, Velocity XF
    if(this.number == CCXF) {
        lastXF[this.port][this.channel] = this.value;
    }
    
    // portamento volume
    if(this.number == CCPORT) {
        lastPortamento[this.port][this.channel] = this.value;
    }
    
    this.sendAfterMilliseconds(LOOKAHEAD);
    
};

// Handle NoteOn messages
NoteOn.prototype.handle = function() {
console.log(JSON.stringify(this));
    // handle stealthy NoteOff
    if(this.velocity < 1) {
        this.sendAfterMilliseconds(LOOKAHEAD);
        return;
    }
    
    /****************************************
     * set flag if articulation has 
     * changed since last note on this channel
     *****************************************/

    let last = lastArt[this.port][this.channel];
    lastArt[this.port][this.channel] = this.articulationID;
    if(last != this.articulationID) {
        this.artChanged = true;
    }
    else {
        this.artChanged = false;
    }

 
    /****************************************
     * handle articulation switches by ID
     * and determine latency
     *****************************************/

    switch(this.articulationID) {
    case 1:
        doSustain(this);
        break;
    case 2:
        doLegato(this);
        break;
    case 3:
        doStaccato(this);
        break;
    case 4:
        doStaccatissimo(this);
        break;
    case 5:
        doSpiccato(this);
        break;
    case 6:
        doSforzando(this);
        break;
    case 7:
        doPizzicato(this);
        break;
    case 8:
        doBartok(this);
        break;
    case 9:
        doColLegno(this);
        break;
    case 10:
        doMarcato(this);
        break;
    case 11:
        doMarcatoLegato(this);
        break;
    case 12:
        doMarcatoOverlay(this);
        break;
    case 13:
        doMarcatoOverlayLegato(this);
        break;
    case 14:
        doHarmonic(this);
        break;
    case 15:
        doTrem(this);
        break;
    case 16:
        doMeasuredTrem(this);
        break;
    case 17:
        doTrill(this);
        break;
    case 18:
        doShortsByMod(this);
        break;
    case 19:
        doPlucksbyMod(this);
        break;
    default:
        this.sendAfterMilliseconds(LOOKAHEAD);
        return;
    }
console.log(JSON.stringify(this));
    // Send actual note
    this.sendAfterMilliseconds(LOOKAHEAD-this.latency);
};



/*********************************************
 * handlers for each articulation
 *********************************************/



// sustain
function doSustain(evt) {
    evt.latency = ZERO;
    sendXF(evt);
    sendSwitch(evt, "C0", 100); // Sustain
    sendSwitch(evt, "A#0",50);  // Legato Off 
}

// Legato
function doLegato(evt) {

    // If this is legato contiuation, change latency
    // base on velocity
    if(evt.artChanged == false) {
 
        if(evt.velocity <= 20) {
            evt.latency = PORTAMENTO;
            sendPortamento(evt);
        }
        else if(evt.velocity >20 && evt.velocity <= 64) {
            evt.latency = SLOW;
        }
        else if(evt.velocity > 64 && evt.velocity <= 100) {
            evt.latency = MEDIUM;
        }
        else if(evt.velocity > 100) {
            evt.latency = FAST;
        }
    }
    else {
        evt.latency = ZERO;
    }

    sendXF(evt);
    sendSwitch(evt, "C0", 100); // Sustain
    sendSwitch(evt, "A#0",100); // Legato On  
}


// Staccato
function doStaccato(evt) {
    evt.latency = SHORT;
    sendSwitch(evt, "F0", 70); 
}

// Staccatissimo
function doStaccatissimo(evt) {
    evt.latency = SHORT;
    sendSwitch(evt, "F0", 50); 
}

// Spiccato
function doSpiccato(evt) {
    evt.latency = SHORT;
    sendSwitch(evt, "F0", 30);  
}

// Sforzando
function doSforzando(evt) {
    evt.latency = SHORT;
    sendSwitch(evt, "F0", 100);
}

// Pizzicato
function doPizzicato(evt) {
    evt.latency = SHORT;
    sendSwitch(evt, "G0", 40); 
}

// Bartok
function doBartok(evt) {
    evt.latency = SHORT;
    sendSwitch(evt, "G0", 60);
}

// Col Legno
function doColLego(evt) {
    evt.latency = SHORT;
    sendSwitch(evt, "G0", 100);
}

// Marcato
function doMarcato(evt) {
    evt.latency = 0;
    sendXF(evt);
    sendSwitch(evt, "F#0", 50);
    sendSwitch(evt, "A#0",50);   
}

// Marcato Legato
function doMarcatoLegato(evt) {
    if(evt.artChanged == true) evt.latency = 0;
    else evt.latency = 50;
    evt.latency = 0;
    sendXF(evt);
    sendSwitch(evt, "F#0", 50);
    sendSwitch(evt, "A#0",100);   
}

// Marcato Overlay
function doMarcatoOverlay(evt) {
    evt.latency = 0;
    sendXF(evt);
    sendSwitch(evt, "F#0", 100);
    sendSwitch(evt, "A#0",50);   
}

// Marcato Legato Overlay
function doMarcatoOverlayLegato(evt) {
    if(evt.artChanged == true) evt.latency = 0;
    else evt.latency = 50;
    sendXF(evt);
    sendSwitch(evt, "F#0", 100);
    sendSwitch(evt, "A#0",100);   
}

// Harmonic
function doHarmonic(evt) {
    evt.latency = ZERO;
    sendXF(evt);
    sendSwitch(evt, "E0", 100);
}

// Tremolo
function doTrem(evt) {
    evt.latency = ZERO;
    sendXF(evt);
    sendSwitch(evt, "C#0", 50);   
}

// Measured Trem
function doMeasuredTrem(evt) {
    evt.latency = ZERO;
    sendXF(evt);
    sendSwitch(evt, "D0", 50);
}

// Trill
function doTrill(evt) {
    evt.latency = 50;
    sendXF(evt);
    sendSwitch(evt, "D#0", 50); 
}

/***********************
 * sendSwitch()
 ***********************/

var ks = new NoteOn;
function sendSwitch(evt, pitchname, vel) {

    // if this is same articulation as previous
    // note, just skip the keyswitches
    if(evt.artChanged==false) {
        return;
    }

    ks.port = evt.port;
    ks.channel = evt.channel;
    ks.pitch = MIDI.noteNumber(pitchname);
    ks.velocity = vel;
    ks.sendAfterMilliseconds(LOOKAHEAD-evt.latency); 
    ks.velocity = 0;
    ks.sendAfterMilliseconds(LOOKAHEAD-evt.latency+1); 
}


// TODO, make this more efficient because not always
//       needed, especially portamento
//       only send when the actual incoming value 
//       has changed for one thing.

/***********************
 * sendXF() 
 ***********************/

var cc = new ControlChange;
function sendXF(evt) {

    // CC1 XF
    if(lastXF[evt.port][evt.channel] != undefined) {
        cc.number = CCXF;
        cc.value = lastXF[evt.port][evt.channel];
        cc.port = evt.port;
        cc.channel = evt.channel;
        cc.sendAfterMilliseconds(LOOKAHEAD-evt.latency);  
    }
}

/***********************
 * sendPortamento() 
 ***********************/

function sendPortamento(evt) {

    // Poratmento Volume
    // TODO don't always send this, only before actual portment notes.

    cc.number = CCPORT;
    cc.value = lastPortamento[evt.port][evt.channel];
    cc.port = evt.port;
    cc.channel = evt.channel;
    cc.sendAfterMilliseconds(LOOKAHEAD-evt.latency); 
}


/**********************
 *   ____ _   _ ___ 
 *  / ___| | | |_ _|
 * | |  _| | | || | 
 * | |_| | |_| || | 
 *  \____|\___/|___|
 *                 
 **********************/
 
// Some enums for the GUI

var PluginParameters = [];

PluginParameters.push({
    type: "checkbox",
    name: "Record-Thru",
    defaultValue: 0,
    disableAutomation: false,
    hidden: false
});
const THRU = PluginParameters.length-1;

PluginParameters.push({
    type: "lin",
    name: "MIDI Port",
    minValue: 1,
    maxValue: MAXPORTS,
    numberOfSteps: MAXPORTS-1,
    defaultValue: 1,
    disableAutomation: false,
    hidden: false
});
const PORT = PluginParameters.length-1;


PluginParameters.push({
    type: "lin",
    name: "MIDI Channel",
    minValue: 1,
    maxValue: MAXMIDI,
    numberOfSteps: MAXMIDI-1,
    defaultValue: 1,
    disableAutomation: false,
    hidden: false
});
const CHANNEL = PluginParameters.length-1;


function ParameterChanged(id, val) {
    GuiParameters.cache(id, val);
}

var GuiParameters = {
    data: [],
    cache: function(id, val) {
        this.data[id] = val;
    },
    get: function(id) {
        if(this.data[id] == undefined) {
            this.data[id] = GetParameter(id);
        }
        return this.data[id];
    },
    check: function(id) {
        return this.data[id];
    }
};

/*******************
 * buffered logging
 *******************/

var console = {
    maxFlush: 20,
    b:[],
    log: function(msg) {this.b.push(msg)},
    flush: function() {
        let i=0;
        while(i<=this.maxFlush && this.b.length>0) {
            Trace(this.b.shift());
            i++;
        }
    }
};


function Idle() {
    console.flush();
}


function Reset() {
    for(let p=1; p<MAXPORTS; p++) {
        for(let c=1; c<=MAXMIDI; c++) 
        lastArt[p][c] = 0;
    }
}

